export class Profile {
    id: number;
    emp_id: string;
    first_name: string;
    last_name: string;
    department: number;
    designation: string;
    supervisor: string;
    hr_master: string;
    image: string;
    qr_code: string
}